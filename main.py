import os
import cv2
import compare
from capture import capture

img = cv2.imread(os.path.join('validation', 'ImageValidationHandler0.jpg'), 0)
text =  capture(img)

for t in text:
    print(compare.compare(t))

