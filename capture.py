import cv2
import numpy as np
import os

def capture(img):
    # img = cv2.imread(os.path.join('validation', f), 0)

    # MedianBlur to remove noise
    blur = cv2.medianBlur(img, 3)

    # Filter nouse by setting threshold
    ret,thresh1 = cv2.threshold(blur, 130, 255, cv2.THRESH_BINARY)

    # Find Contours point values in image 
    contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Find proper coutours in previous result
    # Condition width: 5 < width < 27, height: > 5
    text = []
    point = []
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        if 5 < w < 27 and h > 5:
            point.append([x, y, w, h])
        elif w > 27 and h > 5:
            point.append([x, y, int(w / 2), h])
            point.append([x + int(w / 2) + 1, y, int(w / 2), h])

    # Sort the numbers by the first x-axis value
    point.sort(key = lambda point:point[0])

    for p in point:
        text.append(blur[p[1]:p[1]+p[3], p[0]:p[0]+p[2]])
    return text

def capture_and_save(img):
    files = os.listdir('validation')
    for f in files:
        # Read image in GrayScale mode (Set to 0)
        img = cv2.imread(os.path.join('validation', f), 0)

        # MedianBlur to remove noise
        blur = cv2.medianBlur(img, 3)

        # Filter nouse by setting threshold
        ret,thresh1 = cv2.threshold(blur, 130, 255, cv2.THRESH_BINARY)

        # Find Contours point values in image 
        contours, hierarchy = cv2.findContours(thresh1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Find proper coutours in previous result
        # Condition width: 5 < width < 27, height: > 5
        text = []
        point = []
        for cnt in contours:
            x, y, w, h = cv2.boundingRect(cnt)
            if 5 < w < 27 and h > 5:
                point.append([x, y, w, h])
                print(x, y, w, h)
            elif w > 27 and h > 5:
                point.append([x, y, int(w / 2), h])
                point.append([x + int(w / 2) + 1, y, int(w / 2), h])

        # Sort the numbers by the first x-axis value
        point.sort(key = lambda point:point[0])

        # Save image
        for p in point:
            text.append(blur[p[1]:p[1]+p[3], p[0]:p[0]+p[2]])
            cv2.imwrite('characters/'+f+str(point.index(p))+'.jpg', blur[p[1]:p[1]+p[3], p[0]:p[0]+p[2]])
